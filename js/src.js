const coin = {
    state: 0,
    flip: function() {
        this.state = Math.floor(Math.random() * 2);
    },
    toString: function() {
        return this.state ? 'Heads' : 'Tails';
    },
    toHTML: function() {
        const image = document.createElement('img');
        image.height = 100;
        image.width = 100;
        image.src = this.state ? 'https://meridiancoin.com/wp-content/uploads/2016/06/1970error.jpg' : 'http://www.coinfacts.com/error_coins/mules/1965_two_tailed_quarter_dollar_rev.jpg';
        return image;
    }
};

for (let i = 0; i < 20; i++) {
    coin.flip();
    document.getElementsByClassName('text')[0].append(coin.toString() + ", ");
}
for (let i = 0; i < 20; i++) {
    coin.flip();
    document.getElementsByClassName('picture')[0].append(coin.toHTML());
}